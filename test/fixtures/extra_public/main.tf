/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# provider "random" {
#   version = "~> 2.0"
# }
resource "random_pet" "cow" {
  length    = 2
  prefix    = "cow"
  separator = "-"
}

locals {
  extra_buckets = {
    "cow" = {
      bucket_name   = random_pet.cow.id,
      labels        = { use : "cow" },
      force_destroy = false,
      storage_class = "STANDARD",
      public        = true
    }
  }
}


module "exta_buckets" {
  source                = "../../../examples/extra"
  project_id            = var.project_id
  gcs_location          = "australia-southeast1"
  labels                = var.labels
  extra_buckets         = local.extra_buckets
  create_common_buckets = false
}
