terraform {
  experiments = [module_variable_optional_attrs]
}
variable "project_id" {
  type        = string
  description = "The project in which the buckets will be created"
}

variable "labels" {
  description = "labels to be applied"
  type        = map(any)
  default     = {}
}

variable "create_common_buckets" {
  type        = bool
  default     = true
  description = "If set to false the common buckets (data, code) are not created"
}

variable "extra_buckets" {
  type = map(object({
    bucket_name   = string,
    labels        = optional(map(any)),
    force_destroy = optional(bool),
    storage_class = optional(string)
    public        = optional(bool)
  }))
  default     = {}
  description = "A mapping of extra buckets to be created"
}

variable "gcs_location" {
  type        = string
  description = "The location of the bucket"
  default     = "australia-southeast1"

}
