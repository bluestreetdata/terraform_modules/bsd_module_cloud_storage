# bsd-cloudstorage

This module was generated from [bsd-module-template](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_template), which by default creates a collection of datasets in bigquery. As the module develops, this README should be updated.

The resources/services/activations/deletions that this module will create/trigger are:

- 3 GCS buckets (Data and code) in provided project

## Usage

Basic usage of this module is as follows:

```hcl
module "bsd_cloud_storage" {
  source = "git::git@gitlab.com:bluestreetdata/terraform_modules/bsd_module_cloud_storage.git//?ref=tags/v0.0.1"
  project_id  = "<PROJECT ID>"
}
```

Functional examples are included in the
[examples](./examples/) directory.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| create\_common\_buckets | If set to false the common buckets (data, code) are not created | `bool` | `true` | no |
| extra\_buckets | A mapping of extra buckets to be created | <pre>map(object({<br>    bucket_name   = string,<br>    labels        = optional(map(any)),<br>    force_destroy = optional(bool),<br>    storage_class = optional(string)<br>    public        = optional(bool)<br>  }))</pre> | `{}` | no |
| gcs\_location | The location of the bucket | `string` | `"australia-southeast1"` | no |
| labels | labels to be applied | `map(any)` | `{}` | no |
| project\_id | The project in which the buckets will be created | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| common\_bucket\_names | The names of the made common buckets |
| extra\_bucket\_names | The names of the made extra buckets |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Requirements

These sections describe requirements for using this module.

### Software

The following dependencies must be available:

- [Terraform][terraform] v0.12
- [Terraform Provider for GCP][terraform-provider-gcp] plugin v2.0

### Service Account

A service account with the following roles must be used to provision
the resources of this module:

- Storage Admin: `roles/storage.admin`

The [Project Factory module][project-factory-module] and the
[IAM module][iam-module] may be used in combination to provision a
service account with the necessary roles applied.

### APIs

A project with the following APIs enabled must be used to host the
resources of this module:

- Google Cloud Storage JSON API: `storage-api.googleapis.com`

The [Project Factory module][project-factory-module] can be used to
provision a project with the necessary APIs enabled.

## Contributing

Refer to the [contribution guidelines](./CONTRIBUTING.md) for
information on contributing to this module.

[iam-module]: https://registry.terraform.io/modules/terraform-google-modules/iam/google
[project-factory-module]: https://registry.terraform.io/modules/terraform-google-modules/project-factory/google
[terraform-provider-gcp]: https://www.terraform.io/docs/providers/google/index.html
[terraform]: https://www.terraform.io/downloads.html
